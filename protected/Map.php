<?php


class Map{
    
    public $width = 0;
    public $height = 0;
    public $startPos = array();
    public $finishLine = array();
    public $border = array();
    public $boxes = array();
    public $spheres = array();
    public $toruses = array();
    
}
