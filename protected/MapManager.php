<?php


class MapManager{
    
    protected static $COLOR_BOX = array(0,0,0,0); //black
    protected static $COLOR_SPHERE = array(255,255,0,0); //yellow
    protected static $COLOR_TORUS = array(255,0,255,0); //pink
    protected static $COLOR_START_POS = array(0,255,0,0); //green
    protected static $COLOR_FINISH_POS = array(255,0,0,0); //red
    protected static $COLOR_EMPTY = array(255,255,255,0); //white
    protected static $COLOR_BORDER = array(0,0,255,0); //blue
    
    public function getMapFromImg($imgMapFile){
        $img = $this->_getImgResource($imgMapFile);
        if (is_resource($img)){
            $rawData = $this->_readPixels($img);
            $map = new Map;
            $map->width = imagesx($img);
            $map->height = imagesy($img);
            $this->_fillMapWithRawData($map, $rawData);
            return $map;
        }
    }
    
    protected function _getImgResource($imgFile){
        $img = imagecreatefrompng($imgFile);
        return $img;
    }
    
    protected function _readPixels($img) {
        $w = imagesx($img);
        $h = imagesy($img);
        $rawData = array();
        $pixelsForSkip = array();
        for ($y = 0; $y < $h; $y++) {
            for ($x = 0; $x < $w; $x++) {
                if (!in_array($x . '.' . $y, $pixelsForSkip)) {
                    $rgb = $this->_readPixel($img, $x, $y);
                    if ($rgb !== self::$COLOR_EMPTY) {
                        $rawData[] = array($x, $y, $rgb);
                        $pixelsForSkip[] = $x . '.' . $y;
                        $pixelsForSkip[] = ($x + 1) . '.' . $y;
                        $pixelsForSkip[] = $x . '.' . ($y + 1);
                        $pixelsForSkip[] = ($x + 1) . '.' . ($y + 1);
                    }
                }
            }
        }
        return $rawData;
    }
    
    protected function _readPixel($img, $x, $y){
        $rgb = imagecolorat($img, $x, $y);
        $colors = array_values(imagecolorsforindex($img, $rgb));
        return $colors;
    }
    
    protected function _fillMapWithRawData(Map &$map, $rawData){
        foreach ($rawData as $point){
            $x = $point[0];
            $y = $map->height-$point[1]-1;
            $rgb = $point[2];
            switch ($rgb){
                case self::$COLOR_START_POS:
                    $map->startPos = array($x, $y);
                    break;
                case self::$COLOR_FINISH_POS:
                    $map->finishLine[] = array($x, $y);
                    break;
                case self::$COLOR_BOX:
                    $map->boxes[] = array($x, $y);
                    break;
                case self::$COLOR_SPHERE:
                    $map->spheres[] = array($x, $y);
                    break;
                case self::$COLOR_TORUS:
                    $map->toruses[] = array($x, $y);
                    break;
                case self::$COLOR_BORDER:
                    $map->border[] = array($x, $y);
                    break;
                default:
                    break;
            }
        }
        return $map;
    }
    
}
