<?php

spl_autoload_register(function ($className) {
    $classFile = __DIR__ . DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, $className) . '.php';
    if (is_file($classFile)) {
        require_once $classFile;
    }
});