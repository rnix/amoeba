var AMOEBA = function(material) {

    this.mesh = null;
    this.mat = material || new THREE.MeshLambertMaterial({color: 0xffee00});
    
    this.segNum = 30;
    this.transSeries = [];
    this.prePointsThru = [];

    this.baseShapeSeriaUpdateDelay = 2; 
    this.moveShapeSeriaUpdateDelay = 0.4; 
    
    this.lastUpdateTime = null;
    this.shapeSeriaUpdateDelay = this.baseShapeSeriaUpdateDelay; //s
    this.preSeria = this.getRandomSeria();
    this.restrictions = {};

    this.amoebaX = 0;
    this.amoebaY = 0;

    this.needMove = false;
    this.moveAngle = null;
    this.movementInterval = 20;
    this.moveAngleRestrictions = {};
    this.moveSpeed = 0.4;
    this.moveTimer = 0;
    this.lastMoveVector = 0;
    this.lastMoveVelocity = 0;
    this.inertNeedMove = false;
    this.inertTimer = 0;
    
    this.collideMeshes = [];
    this.collided = false;
    this.collidedSegments = {};
    this.restrictVectors = [];
    this.collideOffset = 0.05;
    this.minDistancesToObjects = {};
    this.moveVectorRestrictThreshold = 0.25;
    
    this.maxFootLength = 10;
    this.minFootLength = 1;
    this.lastUpdateMeshTime = 0;
    this.materialOffsetX = 0;
    this.materialOffsetY = 0;
    this.uniqMaterialOffset = Math.random() * 100;
    
    this.extrudeSettings = {
        amount: 1,
        bevelEnabled: true,
        bevelSegments: 1,
        steps: 1,
        bevelSize: 0.3,
        bevelThickness: 1
    };

    this.sceneMeshUpdateDelay = 40;
}

//+ Jonas Raoni Soares Silva
//@ http://jsfromhell.com/array/shuffle [v1.0]
//v1.0
AMOEBA.prototype.shuffle = function(o) {
    for (var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
}

AMOEBA.prototype.objectSize = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key))
            size++;
    }
    return size;
}

AMOEBA.prototype.getRandomSeria = function() {
    var S = 1000, n = this.segNum;
    var m = S / n;
    var i, r, sign;
    var seria = [];

    for (i = 0; i < n; i++) {
        if (i % 2 === 0) {
            r = THREE.Math.randInt(0, m);
            sign = 1;
        } else {
            sign = -1;
        }
        seria.push(m + sign * r);
    }
    seria = this.shuffle(seria);

    for (i = 0; i < n; i++) {
        seria[i] = (95 + seria[i]) * 0.04;
    }

    return seria;
}

/**
 * Calculates transition series between other 2.
 * 
 * @param {type} seria1
 * @param {type} seria2
 * @returns {Array}
 */
AMOEBA.prototype.getTransitionSeries = function(seria1, seria2) {
    var steps = 50;
    var i, t, s1, s2, step, iterS;
    var transSeries = [];
    for (t = 0; t < steps; t++) {
        transSeries[t] = [];
        for (i = 0; i < seria1.length; i++) {
            s1 = seria1[i];
            s2 = seria2[i];
            step = (s2 - s1) / steps;
            iterS = s1 + t * step;
            transSeries[t][i] = iterS;
        }
    }
    return transSeries;
}

/**
 * 
 * Applies restrictions to seria
 * 
 * @param {type} seria
 * @param {type} restrictions
 * @returns {_L1.recalcSeria.seria}
 */
AMOEBA.prototype.recalcSeria = function(seria, restrictions) {
    var i, s, freeSize;
    var rest = 0;
    var appliedSegments = {};
    for (i in seria) {
        s = seria[i];
        if (typeof restrictions[i] === "number") {
            rest += s - restrictions[i];
            seria[i] = restrictions[i];
            appliedSegments[i] = true;
        }
        if (typeof this.minDistancesToObjects[i] === "number") {
            var val = Math.min(seria[i], this.minDistancesToObjects[i]);
            if (Math.abs(seria[i] - val) > 0.2) {
                rest += seria[i] - val;
                appliedSegments[i] = true;
            }
            seria[i] = val;
        }
    }
    freeSize = this.segNum - this.objectSize(appliedSegments);
    for (i = 0; i < seria.length; i++) {
        if (typeof appliedSegments[i] === "undefined") {
            seria[i] += rest / freeSize;
        }
    }
    
    for (i = 0; i < seria.length; i++) {
        seria[i] = Math.min(seria[i], this.maxFootLength);
    }
    
    return seria;
}

AMOEBA.prototype.recalcSeriaWithCollisionRestrictions = function(seria) {
    for (var segment = 0; segment < seria.length; segment++) {
        if (typeof this.minDistancesToObjects[segment] === "number") {
            seria[segment] = Math.min(seria[segment], this.minDistancesToObjects[segment] - this.collideOffset);
        }
    }
    return seria;
}

AMOEBA.prototype.setPosition = function(pos){
    this.amoebaX = pos.x;
    this.amoebaY = pos.y;
}

AMOEBA.prototype.startMovement = function(angle) {
    this.moveAngle = angle;
    if (this.needMove) {
        return;
    }
    this.moveTimer = 0;
    this.needMove = true;
}

AMOEBA.prototype.stopMovement = function() {
    if (!this.needMove) {
        return;
    }
    this.needMove = false;
    this.moveAngleRestrictions = [];
    this.moveAngle = null;
    this.inertTimer = 0;
    this.inertNeedMove = true;
}

AMOEBA.prototype.inertMove = function(delta) {
    var maxInertTimer = 1;
    var moveVelocity = this.lastMoveVelocity * (1 - this.inertTimer / maxInertTimer);
    this.amoebaX += this.moveSpeed * moveVelocity * this.lastMoveVector.x * (30 * delta);
    this.amoebaY += this.moveSpeed * moveVelocity * this.lastMoveVector.y * (30 * delta);
    this.inertTimer += delta;
    if (this.inertTimer > maxInertTimer) {
        this.inertNeedMove = false;
    }
    if (this.shapeSeriaUpdateDelay < this.baseShapeSeriaUpdateDelay){
        this.shapeSeriaUpdateDelay = Math.max(this.shapeSeriaUpdateDelay, this.baseShapeSeriaUpdateDelay * this.inertTimer/maxInertTimer);
        this.lastUpdateTime -= this.shapeSeriaUpdateDelay * 1000 / 30;
    }
}

AMOEBA.prototype.move = function(delta) {
    var fi = this.moveAngle * 2 * Math.PI / 360;
    var segment = Math.round(this.moveAngle / 360 * this.segNum);
    var segments = this.getNearestSegments(segment, 0.07);
    var i, s;
    for (i in this.moveAngleRestrictions) {
        if (typeof this.moveAngleRestrictions[i] === "number" && typeof this.restrictions[i] === "number") {
            this.restrictions[i] -= this.moveAngleRestrictions[i];
            if (this.restrictions[i] === 0) {
                delete this.restrictions[i];
            }
        }
    }
    this.moveAngleRestrictions = {};
    for (i = 0; i < segments.length; i++) {
        s = segments[i];
        this.moveAngleRestrictions[s] = 4 + 2 * THREE.Math.randInt(0, 2);
        if (typeof this.restrictions[s] !== "nubmer") {
            this.restrictions[s] = this.moveAngleRestrictions[s];
        }
    }

    this.moveTimer += delta;
    var moveVelocity = 0;
    var maxMoveTimer = 1;
    if (this.moveTimer > maxMoveTimer) {
        moveVelocity = 1;
    } else {
        moveVelocity = this.moveTimer / maxMoveTimer;
    }
    this.shapeSeriaUpdateDelay = this.baseShapeSeriaUpdateDelay - moveVelocity * (this.baseShapeSeriaUpdateDelay - this.moveShapeSeriaUpdateDelay);
    var moveVector = new THREE.Vector2(Math.sin(fi), Math.cos(fi)).normalize();
    if (this.restrictVectors.length) {
        for (var i = 0; i < this.restrictVectors.length; i++) {
            var restrictVector = this.restrictVectors[i].normalize();
            if (Math.abs(moveVector.x - restrictVector.x) < this.moveVectorRestrictThreshold) {
                moveVector.x = 0;
            }
            if (Math.abs(moveVector.y - restrictVector.y) < this.moveVectorRestrictThreshold) {
                moveVector.y = 0;
            }
        }
    }
    this.amoebaX += this.moveSpeed * moveVelocity * moveVector.x * (30 * delta);
    this.amoebaY += this.moveSpeed * moveVelocity * moveVector.y * (30 * delta);
    this.lastMoveVector = moveVector;
    this.lastMoveVelocity = moveVelocity;
}

AMOEBA.prototype.getNearestSegments = function(segment, offset) {
    var i;
    var offsetNum = Math.round(offset * this.segNum);
    var segments = [];
    for (i = 0; i <= offsetNum; i++) {
        segments.push(segment + i);
        segments.push(segment - i);
    }
    for (i = 0; i < segments.length; i++) {
        if (segments[i] > this.segNum) {
            segments[i] = this.segNum - segments[i];
        } else if (segments[i] < 0) {
            segments[i] = this.segNum + segments[i];
        }
    }

    return segments;
}

AMOEBA.prototype.calculateMovementRestrictVectors = function() {
    var segment, fi;
    this.restrictVectors = [];
    for (segment in this.minDistancesToObjects){
        if (this.minDistancesToObjects[segment] < this.minFootLength){
            fi = 2 * Math.PI * segment / (this.segNum - 1);
            this.restrictVectors.push(new THREE.Vector2(Math.sin(fi), Math.cos(fi)));
        }
    }
}

/* Throws rays from center through shape points */
AMOEBA.prototype.determineCollisions = function() {
    if (this.prePointsThru) {
        var foundCollision = false;
        var originPoint = new THREE.Vector3(this.amoebaX, this.amoebaY, 0);
        this.collidedSegments = {};
        this.minDistancesToObjects = {};
        var ray = new THREE.Raycaster();
        for (var segment = 0; segment < this.prePointsThru.length; segment++) { 
            var directionVector = new THREE.Vector3(this.prePointsThru[segment].x, this.prePointsThru[segment].y, 0);
            ray.set(originPoint, directionVector.clone().normalize());
            var collisionResults = ray.intersectObjects(this.collideMeshes);
            if (collisionResults.length > 0 && collisionResults[0].distance < directionVector.length() + this.collideOffset) {
                foundCollision = true;
                this.collidedSegments[segment] = collisionResults[0].distance;
                this.collided = true;
            }

            this.calculateMinimalDistancesToObstacles(segment, collisionResults);
        }
        
        if (!foundCollision && this.collided) {
            this.collided = false;
        }
    }
}

/* calc min distances to nearest objects */
AMOEBA.prototype.calculateMinimalDistancesToObstacles = function(segment, rayIntersects) {
    if (rayIntersects.length > 0) {
        var minDistance = 1000;
        for (var i = 0; i < rayIntersects.length; i++) {
            if (rayIntersects[i].distance < minDistance) {
                minDistance = rayIntersects[i].distance;
            }
        }
        if (minDistance < 1000) {
            this.minDistancesToObjects[segment + 1] = minDistance;
            if (segment === 0) {
                this.minDistancesToObjects[0] = minDistance;
            }
        }
    }
}

AMOEBA.prototype.getShapeSeria = function() {
    /* smooth wave effects */
    var seria, seria2;
    var now = new Date().getTime();
    if (!this.lastUpdateTime || this.lastUpdateTime < now - this.shapeSeriaUpdateDelay * 1000) {
        seria2 = this.getRandomSeria();
        seria2 = this.recalcSeria(seria2, this.restrictions);
        this.transSeries = this.getTransitionSeries(this.preSeria, seria2);
        this.preSeria = seria2;
        this.lastUpdateTime = now;
    }
    var ind = Math.floor(((now - this.lastUpdateTime) / (this.shapeSeriaUpdateDelay * 1000)) * (this.transSeries.length - 1));
    seria = this.transSeries[ind];
    seria = this.recalcSeriaWithCollisionRestrictions(seria);
    
    return seria;
}

AMOEBA.prototype.getShape = function() {
    var pointsThru = [];
    var x, y, fi;
    var shape = new THREE.Shape();
    var firstDraw = true;
    var seria = this.getShapeSeria();
    for (var segment = 0; segment < this.segNum; segment++) {
        var fi = segment / (this.segNum) * 2 * Math.PI;
        var radius = seria[segment];
        x = radius * Math.cos(Math.PI / 2 - fi);
        y = radius * Math.sin(Math.PI / 2 - fi);

        if (firstDraw) {
            shape.moveTo(x, y);
            firstDraw = false;
        } else {
            pointsThru.push(new THREE.Vector2(x, y));
            //shape.lineTo(x, y);
        }
    }
    shape.splineThru(pointsThru);
    this.prePointsThru = pointsThru;
    
    return shape;
}

AMOEBA.prototype.updateMaterial = function() {
    if (typeof this.mat.map === 'undefined' || !this.mat.map){
        return;
    }
    
    if (this.moveAngle === null){
        var now = new Date().getTime();
        this.materialOffsetX += Math.sin(this.uniqMaterialOffset + now/500) *  THREE.Math.randFloat(0.5, 1.5) * 0.03;
        this.materialOffsetY += Math.cos(this.uniqMaterialOffset + now/500) * Math.sin(now/300) * THREE.Math.randFloat(0.5, 1.5) * 0.03 ;
    } else {
        var fi = this.moveAngle * 2 * Math.PI / 360;
        var moveVector = new THREE.Vector2(Math.sin(fi), Math.cos(fi)).normalize();
        this.materialOffsetX -= moveVector.x * this.moveSpeed * 0.05;
        this.materialOffsetY -= moveVector.y * this.moveSpeed * 0.05;
    }
    
    this.mat.map.offset.set( this.materialOffsetX, this.materialOffsetY );
}

AMOEBA.prototype.update = function(delta, scene) {
    
    this.determineCollisions();
    this.calculateMovementRestrictVectors();
    
    if (this.needMove) {
        this.move(delta);
    }
    if (this.inertNeedMove && !this.needMove) {
        this.inertMove(delta);
    }
    
    var shape = this.getShape();
    var now = new Date().getTime();
    if (!this.lastUpdateMeshTime || this.lastUpdateMeshTime < now - this.sceneMeshUpdateDelay) {
        this.lastUpdateMeshTime = now;
        var geometry = new THREE.ExtrudeGeometry(shape, this.extrudeSettings);
        this.updateMaterial();
        
        if (this.mesh) {
            scene.remove(this.mesh);
        }

        this.mesh = THREE.SceneUtils.createMultiMaterialObject(geometry, [this.mat]);
        this.mesh.castShadows = true;
        scene.add(this.mesh);
        
    }
    
    this.mesh.position.set(this.amoebaX, this.amoebaY, -1.5);
    
    
    this.restrictions = {};
}
