(function() {
    if (!Detector.webgl) {
        Detector.addGetWebGLMessage();
    }

    window.onload = init;

    var container, stats;

    var camera, scene, renderer;


    var mousePushed = false;

    var windowHalfX = window.innerWidth / 2;
    var windowHalfY = window.innerHeight / 2;


    var clock = new THREE.Clock();


    var leftPushed = false;
    var upPushed = false;
    var rightPushed = false;
    var downPushed = false;

    var amoeba;
    var sceneObstacles = [];
    var finishLine = [];
    var cubeSize = 6;
    var amoebaMoveSpeed = 0.4;
    var mapScale = cubeSize / 2;
    
    var screenTimerContainer;
    var screenTimerInterval = 100;
    var screenTimerIntervalId;
    var screenTimerValue = 0;
    var finished = false;


    function init() {

        container = document.getElementById('container');

        camera = new THREE.PerspectiveCamera(25, window.innerWidth / window.innerHeight, 1, 10000);
        camera.position.set(0, 0, 40);

        scene = new THREE.Scene();
        scene.fog = new THREE.FogExp2(0xfff4e5, 0.0003);
        camera.lookAt(scene.position);

        // LIGHTS
        var ambient = new THREE.AmbientLight(0x222222);
        scene.add(ambient);
        var directionalLight = new THREE.DirectionalLight(0xffeedd);
        directionalLight.position.set(0, -70, 100).normalize();
        scene.add(directionalLight);

        // GROUND

        var bgTexture = THREE.ImageUtils.loadTexture("img/grass.jpg");
        var planeSimple = new THREE.PlaneGeometry(2000, 2000);
        var planeMat = new THREE.MeshPhongMaterial({color: 0xffffff, map: bgTexture});
        var floor = new THREE.Mesh(planeSimple, planeMat);
        floor.material.map.repeat.set(120, 120);
        floor.material.map.wrapS = floor.material.map.wrapT = THREE.RepeatWrapping;
        floor.receiveShadow = true;
        scene.add(floor);


        renderer = new THREE.WebGLRenderer({clearColor: 0xffffff, clearAlpha: 1, antialias: true});
        renderer.setSize(window.innerWidth, window.innerHeight);
        renderer.setClearColor(scene.fog.color, 1);
        renderer.sortObjects = false;

        container.appendChild(renderer.domElement);

        stats = new Stats();
        stats.domElement.style.position = 'absolute';
        stats.domElement.style.top = '0px';
        container.appendChild(stats.domElement);

        
        // CONTROLS

        window.addEventListener('resize', onWindowResize, false);

        window.addEventListener('keydown', function(event) {
            changeKeyState(event.which, true);
        });

        window.addEventListener('keyup', function(event) {
            changeKeyState(event.which, false);
        });
        renderer.domElement.addEventListener('mousemove', onDocumentMouseMove, false);
        renderer.domElement.addEventListener('mousedown', onDocumentMouseDown, false);
        renderer.domElement.addEventListener('mouseup', onDocumentMouseUp, false);
        
        
        var boxGeometry = new THREE.CubeGeometry(cubeSize, cubeSize, cubeSize);
        var cubeMaterials = [];
        var cubeMat1 = new THREE.MeshLambertMaterial({
            map: THREE.ImageUtils.loadTexture("img/stone1.jpg")
        });
        cubeMaterials.push(cubeMat1);
        var cubeMat2 = new THREE.MeshLambertMaterial({
            map: THREE.ImageUtils.loadTexture("img/stone2.jpg")
        });
        cubeMaterials.push(cubeMat2);
        var cubeMat3 = new THREE.MeshLambertMaterial({
            map: THREE.ImageUtils.loadTexture("img/stone3.jpg")
        });
        cubeMaterials.push(cubeMat3);
        
        var sphereGeometry = new THREE.SphereGeometry( 3, 6, 12 );
        var sphereMat = new THREE.MeshLambertMaterial({
            map: THREE.ImageUtils.loadTexture("img/green.jpg")
        });
        
        var torusMaterials = [];
        var torusGeometry = new THREE.TorusGeometry( 2, 2, 12, 4 );
        var torusMat1 = new THREE.MeshLambertMaterial({
            map: THREE.ImageUtils.loadTexture("img/steel1.jpg")
        });
        torusMaterials.push(torusMat1);
        var torusMat2 = new THREE.MeshLambertMaterial({
            map: THREE.ImageUtils.loadTexture("img/steel2.jpg")
        });
        torusMaterials.push(torusMat2);
        
        var borderGeometry = new THREE.CubeGeometry(cubeSize, cubeSize, 6);
        var cubeMatBorder = new THREE.MeshLambertMaterial({
            map: THREE.ImageUtils.loadTexture("img/border.jpg")
        });
        
        var finishGeometry = new THREE.CubeGeometry(cubeSize, cubeSize, 0.1);
        var cubeMatFinish = new THREE.MeshLambertMaterial({
            map: THREE.ImageUtils.loadTexture("img/finish.jpg")
        });
        

        var mapAmoeba = THREE.ImageUtils.loadTexture("img/bubbles.jpg");
        mapAmoeba.wrapS = mapAmoeba.wrapT = THREE.RepeatWrapping;
        mapAmoeba.repeat.set(0.4, 0.4);


        var material = new THREE.MeshPhongMaterial({
            color: 0xA0B8FF,
            map: mapAmoeba,
            specular: 0x005500,
            opacity: 0.7,
            transparent: true
        });
        amoeba = new AMOEBA(material);
        amoeba.moveSpeed = 0;

        
        $.getJSON( "js/maps/map1.js?v=18", function( map ) {
            amoeba.moveSpeed = amoebaMoveSpeed;
            amoeba.setPosition(new THREE.Vector2(map.startPos[0] * mapScale, map.startPos[1] * mapScale));
            var i, cube;
            
            for (i=0; i<map.border.length; i++){
                cube = new THREE.Mesh(borderGeometry, cubeMatBorder);
                cube.position.set(map.border[i][0] * mapScale, map.border[i][1] * mapScale, 0);
                scene.add(cube);
                sceneObstacles.push(cube);
            }
            
            for (i=0; i<map.boxes.length; i++){
                addObjectFromMap(new THREE.Mesh(boxGeometry, cubeMaterials[THREE.Math.randInt(0,cubeMaterials.length-1)]), map.boxes[i][0], map.boxes[i][1]);
            }
            
            for (i=0; i<map.spheres.length; i++){
                addObjectFromMap(new THREE.Mesh(sphereGeometry, sphereMat), map.spheres[i][0], map.spheres[i][1]);
            }
            
            for (i=0; i<map.toruses.length; i++){
                addObjectFromMap(new THREE.Mesh(torusGeometry, torusMaterials[THREE.Math.randInt(0,torusMaterials.length-1)]), map.toruses[i][0], map.toruses[i][1]);
            }
            
            for (i=0; i<map.finishLine.length; i++){
                finishLine.push(new THREE.Vector2(map.finishLine[i][0] * mapScale, map.finishLine[i][1] * mapScale));
                cube = new THREE.Mesh(finishGeometry, cubeMatFinish);
                cube.position.set(map.finishLine[i][0] * mapScale, map.finishLine[i][1] * mapScale, 0);
                scene.add(cube);
            }
            
        });

        loop();

    }
    
    function addObjectFromMap(mesh, x, y) {
        mesh.position.set(x * mapScale, y * mapScale, -0.1);
        scene.add(mesh);
        sceneObstacles.push(mesh);
    }

    function onWindowResize() {

        windowHalfX = window.innerWidth / 2;
        windowHalfY = window.innerHeight / 2;

        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();

        renderer.setSize(window.innerWidth, window.innerHeight);

    }
    
    function getOnMouseMoveAngle(clickX, clickY) {
        var mouseX = (clickX - windowHalfX);
        var mouseY = -(clickY - windowHalfY);
        var hitPoint = new THREE.Vector3(mouseX, mouseY, 0);
        var relativeVector = hitPoint.sub(amoeba.mesh.position);
        var angle1 = relativeVector.angleTo(new THREE.Vector3(0, 1, 0));
        var angle2 = relativeVector.angleTo(new THREE.Vector3(1, 0, 0));
        if (angle2 > Math.PI / 2) {
            angle1 = 2 * Math.PI - angle1;
        }
        var angle = angle1 / (2 * Math.PI) * 360;
        return angle;
    }

    function onDocumentMouseDown(event) {
        event.preventDefault();
        mousePushed = true;
        amoeba.startMovement(getOnMouseMoveAngle(event.clientX, event.clientY));
        if (!screenTimerIntervalId && !finished){
            startScreenTimer();
        }
    }

    function onDocumentMouseMove(event) {
        if (mousePushed){
            event.preventDefault();
            amoeba.moveAngle = getOnMouseMoveAngle(event.clientX, event.clientY);
        }
    }
    
    function onDocumentMouseUp(event) {
        event.preventDefault();
        amoeba.stopMovement();
        mousePushed = false;
    }


    function changeKeyState(key, state) {
        switch (key) {
            case 37:
                leftPushed = state;
                break;
            case 38:
                upPushed = state;
                break;
            case 39:
                rightPushed = state;
                break;
            case 40:
                downPushed = state;
                break;
            default:
                return;
        }
        moveOnKeyPress();

    }

    function moveOnKeyPress() {
        var angle = null;
        if (leftPushed) {
            angle = 270;
        }
        if (upPushed) {
            angle = 0;
        }
        if (rightPushed) {
            angle = 90;
        }
        if (downPushed) {
            angle = 180;
        }

        if (leftPushed && upPushed) {
            angle = 315;
        }
        if (leftPushed && downPushed) {
            angle = 225;
        }
        if (upPushed && rightPushed) {
            angle = 45;
        }
        if (rightPushed && downPushed) {
            angle = 135;
        }

        if (leftPushed && rightPushed || upPushed && downPushed) {
            angle = null;
        }

        if (angle !== null) {
            amoeba.startMovement(angle);
        } else {
            amoeba.stopMovement();
        }
        
        if (!screenTimerIntervalId && !finished){
            startScreenTimer();
        }
    }

    function loop() {

        requestAnimationFrame(loop, renderer.domElement);

        var delta = clock.getDelta();


        amoeba.collideMeshes = sceneObstacles;
        amoeba.update(delta, scene);

        camera.position = new THREE.Vector3(amoeba.mesh.position.x, amoeba.mesh.position.y, 140);
        camera.lookAt(new THREE.Vector3(amoeba.mesh.position.x, amoeba.mesh.position.y, 0));

        
        renderer.render(scene, camera);
        
        if (checkFinishLine()){
            onFinish();
        }
        
        stats.update();

    }
    
    function startScreenTimer(){
        screenTimerContainer = document.getElementById('screenTimerContainer');
        screenTimerValue = 0;
        window.clearInterval(screenTimerIntervalId);
        screenTimerIntervalId = window.setInterval(updateScreenTimer, screenTimerInterval);
    }

    function updateScreenTimer(){
        if (screenTimerContainer){
            screenTimerValue += screenTimerInterval / 1000;
            screenTimerContainer.innerHTML = screenTimerValue.toFixed(1);
        }
    }
    
    function stopScreenTimer(){
        window.clearInterval(screenTimerIntervalId);
        screenTimerIntervalId = null;
    }
    
    function checkFinishLine(){
        var i, leftSquareVertex;
        for (i=0; i<finishLine.length; i++){
            leftSquareVertex = finishLine[i];
            if (checkSquareCollision(leftSquareVertex)){
                return true;
            }
        }
        return false;
    }
    
    function checkSquareCollision(leftSquareVertex){
        var lt = leftSquareVertex;
        var rt = new THREE.Vector2(lt.x + cubeSize, lt.y);
        var rb = new THREE.Vector2(lt.x + cubeSize, lt.y + cubeSize);
        var lb = new THREE.Vector2(lt.x, lt.y + cubeSize);
        var diag = lt.distanceTo(rb);
        var side = lt.distanceTo(rt);
        
        var point = new THREE.Vector2(amoeba.amoebaX, amoeba.amoebaY);
        
        var d1 = point.distanceTo(lt);
        var d2 = point.distanceTo(rt);
        var d3 = point.distanceTo(rb);
        var d4 = point.distanceTo(lb);
        
        var checkOffset = cubeSize;
        
        if (d1 + d2 + d3 + d4 <= (2*side + diag + checkOffset)){
            return true;
        }
        return false;
    }
    
    function onFinish(){
        amoeba.moveSpeed = 0;
        stopScreenTimer();
        finished = true;
        document.getElementById('resultValue').innerHTML = screenTimerValue.toFixed(1);
        document.getElementById('resultContainer').style.display = 'block';
        
        var resultId = 'bronze';
        if (screenTimerValue < 19){
            resultId = 'gold';
        } else if (screenTimerValue < 22){
            resultId = 'silver';
        }
        document.getElementById('result-' + resultId).style.display = 'block';
    }

})();
