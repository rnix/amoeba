<?php

require_once __DIR__.'/../protected/_autoload.php';
$mapsDir = __DIR__.'/../protected/maps';
$outputDir = __DIR__.'/js/maps';

$mapFile = $mapsDir.'/map1.png';
$mapManager = new MapManager();
$map = $mapManager->getMapFromImg($mapFile);

$json = json_encode($map);
file_put_contents($outputDir.'/map1.js', $json);
echo $json;